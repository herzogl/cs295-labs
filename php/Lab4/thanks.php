<p>
		<h2>Thank You For Submitting <em><?php echo htmlentities($_POST{'title'}); ?></em></h2>	        
	        
	        <h3>Ingredients</h3>
	        <ul>
	        	<li><?php echo htmlentities($_POST{'ingredient0'});?></li>
	        	<li><?php echo htmlentities($_POST{'ingredient1'});?></li>
	        	<li><?php echo htmlentities($_POST{'ingredient2'});?></li>	        
	        </ul>
	        
	        <h3>Instructions</h3>
			
	        <?php echo htmlentities($_POST{'instructions'});?>
	        
							
		<a href="index.php">Return to recipe list</a>	
</p>