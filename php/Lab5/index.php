<?php
require_once("recipemodel.php");
require_once("view.php");
$view = NULL;

	if ($_SERVER['REQUEST_METHOD']== "POST" && $_GET['action']=="insert") {
		$view = new View ('thanks', NULL);
		$view->Render();
	}
	
	elseif (isset($_GET['action']) && $_GET['action'] == "insert") {
		$view = new View ('form', NULL);
		$view->Render();	
	}
		
	else {
		$recipemodel = new Recipemodel();
		$view = new View('list', $recipemodel->findAll());
		
		$view->Render();
		}

?>