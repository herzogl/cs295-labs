<?php
require_once("model.php");

class recipe
{
	public $id;
	public $title;
	public $ingredient0;
	public $ingredient1;
	public $ingredient2;
	public $instructions;
	
function __construct($id, $title, $ing0, $ing1, $ing2, $ins){
	$this->id = $id;
	$this->title = $title;
	$this->ingredient0 = $ing0;
	$this->ingredient1 = $ing1;
	$this->ingredient2 = $ing2;
	$this->instructions = $ins;
	}
}
	
	class recipemodel extends model {

function findAll() {
$data = array(
                    new Recipe("0", "Spaghetti", "Tomatoes", "Garlic", "Basil", "Make it good."),
                    new Recipe("1", "Pad Thai", "Rice noodles", "Tamarind", "Green onions", "Make it good."),
                    new Recipe("2", "White Bean Soup", "Navy beans", "Ham hock", "Broth", "Make it good."));
			return $data;
			}
}