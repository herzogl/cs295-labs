

			<a href="index.php?action=insert">Insert a recipe
			</a>&nbsp;|&nbsp;
			<a href="index.php?action=delete">Delete selected recipe(s)</a>

			<table class="table table-striped">
				<thead>
					<tr>
						<th>Delete?</th>
						<th>#</th>
						<th>Title</th>                  
						<th>Ingredient 1</th>
						<th>Ingredient 2</th>
						<th>Ingredient 3</th>
						<th>Instructions</th>
					</tr>
				</thead>
			<tbody>
			<?php foreach ($this->data as $recipe) {
				?>     
					<tr>
						<td><form method="post"><input type="checkbox" name="delete[]" value="<?php $row['id']; ?>" /></form></td>
						<td><?php echo htmlentities($recipe->id); ?></td>
						<td><?php echo htmlentities($recipe->title); ?></td>                                
						<td><?php echo htmlentities($recipe->ingredient0); ?></td>
						<td><?php echo htmlentities($recipe->ingredient1); ?></td>
						<td><?php echo htmlentities($recipe->ingredient2); ?></td>
						<td><?php echo htmlentities($recipe->instructions); ?></td>
					</tr>                                
			<?php } ?>          
			</tbody>                
			</table>         
			
			

				