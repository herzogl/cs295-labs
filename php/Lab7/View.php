<?php

	class View {
		function __construct($page, $data) {
			$this->data = $data;
			$this->page = $page;     
    }
		function render() {
		ob_start();
		include_once $this->page . '.php';
		$content = ob_get_clean();
		require_once 'layout.php';
	} 
}
?>