<?php

if (isset($_ENV['OPENSHIFT_APP_NAME'])){
    define("DB_NAME", "CS295labs");
    define("DB_HOST", $_ENV['OPENSHIFT_MYSQL_DB_HOST']);
    define("DB_USER", $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
    define("DB_PASS", $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']); 
	}

else {
    define("DB_NAME", "CS295labs");
    define("DB_HOST", "localhost");
    define("DB_USER", "root");
    define("DB_PASS", "");  
	}